let navItems = document.querySelector("#navSession");

// localStorage = an object used to store information in our clients/devices
// make it a point na i clear ang local storage pag may nag lalogout na user
let userToken = localStorage.getItem("token");
console.log(userToken);

// Global variable for key value pairs

// localStorage {	
// 	getItem : function () => {
// 		prints key
// 	}
// }

// render it means you will populate/generate yung item na gusto nating palabasin

// logic = if no user token we will generate a button that will say login
if(!userToken) {
	// We can create/add in the targeted html
	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link">
				login
				</a>
			</li>
		`
} else {
	navItems.innerHTML =
	`
	<li class="nav-item">
				<a href="logout.html" class="nav-link">
				Logout
				</a>
			</li>
	`
}


