let token = localStorage.getItem('token')
let profileContainer = document.querySelector('#profileContainer')

fetch('https://fierce-beach-86967.herokuapp.com/api/users/details', {
	headers: {
				'Authorization': `Bearer ${token}`
					}
})
.then(res => res.json())
.then(data => {
	console.log(data)
	courses = data.enrollments.map(course => {
		fetch(`https://fierce-beach-86967.herokuapp.com/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {
					});
		return (
					`<tr>
						<td>course name:course.name</td>
						<td>date:${course.date}</td>
						<td>status:${course.status}</td>
					</tr>`
				)

	})

	profileContainer.innerHTML = 
	`
		<h3>${data.firstName}</h3>
		<h3>${data.lastName}</h3>
		<h3>${data.email}</h3>

	`
})
